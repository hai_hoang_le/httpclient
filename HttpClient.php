<?php

$client = new HttpClient();
$optionsObject = new HttpOptions();

$context=$optionsObject->setMethod('OPTIONS')
    ->getContext();


$token = $client->sendHttpRequest('https://www.coredna.com/assessment-endpoint.php', $context);

$context=$optionsObject->setMethod('POST')
    ->setContentType('application/json')
    ->setBearerToken($token)
    ->setContent([
        "name" => "Harry Le",
        "email" => "lehoanghai1991@gmail.com",
        "url" => "https://bitbucket.org/hai_hoang_le/httpclient",
    ])->getContext();

$result = $client->sendHttpRequest('https://www.coredna.com/assessment-endpoint.php', $context);

print_r($client->getHeader());


class HttpClient{

    private $header=[];

    /**
     * @return array
     */
    public function getHeader()
    {
        return $this->header;
    }

    public function sendHttpRequest($url,$context){

        $response = file_get_contents($url, false, $context);
        $this->header=$http_response_header;
        if ($response===false) {
            $error = error_get_last();
            throw new Exception("Request issue: ".$error['message']);
        }
        return $response;
    }
}

class HttpOptions{
    private $contentType;
    private $bearerToken;
    private $method;
    private $content=[];

    /**
     * @param string $contentType
     * @return HttpOptions
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * @param string $bearerToken
     * @return HttpOptions
     */
    public function setBearerToken($bearerToken)
    {
        $this->bearerToken = $bearerToken;

        return $this;
    }

    /**
     * @param string $method
     * @return HttpOptions
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @param array $content
     * @return HttpOptions
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Combine data in the object to create context
     *
     * @return resource
     */
    public function getContext(){
        $header="";
        if($this->contentType){
            $header.="Content-Type: ".$this->contentType."\r\n";
        }

        if($this->bearerToken){
            $header.="Authorization: Bearer ".$this->bearerToken."\r\n";
        }
        $array=['http'=>[]];

        if(!empty($header)){
            $array['http']['header']=$header;
        }

        if(!empty($this->method)){
            $array['http']['method']=$this->method;
        }

        if(!empty($this->content)){
            $content =  json_encode($this->content);
            $array['http']['content']=$content;
        }

        return stream_context_create($array);
    }
}


